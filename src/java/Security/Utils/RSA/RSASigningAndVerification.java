/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Security.Utils.RSA;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.X509EncodedKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;


/**
 *
 * @author bobberkarl
 */
public class RSASigningAndVerification {
    
     public static void main(String[] args){
        
         try {
                KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
                kpg.initialize(2048);
                KeyPair keyPair = kpg.genKeyPair();
                String signature = RSASigner("essayons", keyPair.getPrivate());
                System.out.println("Signature: " + signature );
                
                boolean verification = RSAVerify(keyPair.getPublic(), "essayons",new BASE64Decoder().decodeBuffer(signature));
                System.out.println("The verification is " + verification);
                
         }  catch (NoSuchAlgorithmException ex) {
             Logger.getLogger(RSASigningAndVerification.class.getName()).log(Level.SEVERE, null, ex);
         } catch (InvalidKeyException ex) {
             Logger.getLogger(RSASigningAndVerification.class.getName()).log(Level.SEVERE, null, ex);
         } catch (SignatureException ex) {
             Logger.getLogger(RSASigningAndVerification.class.getName()).log(Level.SEVERE, null, ex);
         } catch (UnsupportedEncodingException ex) {
             Logger.getLogger(RSASigningAndVerification.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
             Logger.getLogger(RSASigningAndVerification.class.getName()).log(Level.SEVERE, null, ex);
         }
     }

    
    
    
    // Return  the signature of the message

    public static String RSASigner(String message, PrivateKey privateKey) throws InvalidKeyException, SignatureException, NoSuchAlgorithmException, UnsupportedEncodingException{
        
        
        byte[] data = message.getBytes("UTF8");

        Signature sig = Signature.getInstance("MD5WithRSA");
        sig.initSign(privateKey);
        sig.update(data);
        
        
        byte[] signatureBytes = sig.sign();
       // System.out.println("Signature: " + new BASE64Encoder().encode(signatureBytes));
/*
        sig.initVerify(keyPair.getPublic());
        sig.update(data);

        System.out.println(sig.verify(signatureBytes));
*/
        return new BASE64Encoder().encode(signatureBytes);
    }
    
    
    // TODO RSA verify signature doesn't work, and doesn't output anything
    public static boolean RSAVerify(PublicKey publicKey, String message, byte [] signature){
         try {
             
                Signature sig = Signature.getInstance("MD5WithRSA");
                sig.initVerify(publicKey);
                sig.update(message.getBytes("UTF8"));
                return sig.verify(signature);
                
         } catch (SignatureException ex) {
             Logger.getLogger(RSASigningAndVerification.class.getName()).log(Level.SEVERE, null, ex);
         } catch (InvalidKeyException ex) {
             Logger.getLogger(RSASigningAndVerification.class.getName()).log(Level.SEVERE, null, ex);
         } catch (UnsupportedEncodingException ex) {
             Logger.getLogger(RSASigningAndVerification.class.getName()).log(Level.SEVERE, null, ex);
         } catch (NoSuchAlgorithmException ex) {
             Logger.getLogger(RSASigningAndVerification.class.getName()).log(Level.SEVERE, null, ex);
         }
    
    return false;
    
    }
}
