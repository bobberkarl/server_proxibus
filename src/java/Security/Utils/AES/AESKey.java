/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Security.Utils.AES;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author bobberkarl
 */
public class AESKey {
    
    
    private static String KeyStoreLocation = "/home/bobberkarl/Documents/keys/proxibus/Test/Server_Terminal_Shared_AES_keys/";
    
     public static void main(String[] args){
        
         SecretKey a;
        try {
            a = AESkeyCreation("TEST");
            System.out.println("original  " +Base64.getEncoder().encodeToString(a.getEncoded()));
            
            System.out.println("from file "+Base64.getEncoder().encodeToString(getAESKey("TEST").getEncoded()));
            
            
            
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AESKey.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AESKey.class.getName()).log(Level.SEVERE, null, ex);
        }
     }

   
    
    // Return AESKEY
    public static SecretKey AESkeyCreation(String SecretKeyName) throws NoSuchAlgorithmException, IOException{
    
        // if key exists, return existant key
        File f = new File(KeyStoreLocation+SecretKeyName);
        if(f.exists()){
        return getAESKey(SecretKeyName);
        }
        
        
    KeyGenerator keyGen = KeyGenerator.getInstance("AES");
    keyGen.init(256); // for example
    SecretKey secretKey = keyGen.generateKey();
    
    
    // Store the Secret Key on Disk
    FileUtils.writeByteArrayToFile(new File(KeyStoreLocation+SecretKeyName), secretKey.getEncoded());
   
    
    return secretKey;
    }
    
    public static SecretKey getAESKey(String SecretKeyName) throws IOException{
    
        // get secret key byte array from file
    byte[] a = FileUtils.readFileToByteArray(new File(KeyStoreLocation+SecretKeyName));
   
        // store the byte array in a secret key
    SecretKey originalKey = new SecretKeySpec(a, 0, a.length, "AES");
    
    return originalKey;
    }
    
   
}
