/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Security;

import Security.Utils.AES.AESEncryptor;
import Security.Utils.RSA.KeyReaders.PrivateKeyReader;

import Security.Utils.RSA.RSASigningAndVerification;
import clientDevices.Client;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 *
 * @author bobberkarl
 */
public class AlphaToken {
    static Token token;
    static String TokenSignature;
    static String Time;
    static Client client;
    
    private static String AlphaToken;
    private static String Location = "/home/bobberkarl/Documents/keys/proxibus/Test/CompanyKeys/";
    
     public static void main(String[] args) {
         try {
        token = new Token("CompanyKeykeys");
        TokenSignature = RSASigningAndVerification.RSASigner(token.GetToken(), PrivateKeyReader.get(Location+"CompanyKeykeys") );
        Time = getCurrentTimeStamp();
        client = client;
        AlphaToken = token.GetToken()+token.GetToken().length()+TokenSignature+TokenSignature.length()+Time+client.getClientIDString()+client.getClientIDString().length();
             System.out.println("Alpha Token = " + AlphaToken);
         } catch (Exception e) {
             System.out.println(e);
         }
        
    }
    
    // if access is not granted, don't give any token

    public AlphaToken(Client client,String CompanyID) throws InvalidKeyException, SignatureException, NoSuchAlgorithmException, UnsupportedEncodingException, Exception {
        this.token = new Token(CompanyID);
        this.TokenSignature = RSASigningAndVerification.RSASigner(token.GetToken(), PrivateKeyReader.get(Location+CompanyID) );
        this.Time = getCurrentTimeStamp();
        this.client = client;
        AlphaToken = token.GetToken()+token.GetToken().length()+TokenSignature+TokenSignature.length()+Time+client.getClientIDString()+client.getClientIDString().length();
    
    }

    public void initAlphaToken(Client client,String CompanyID) throws InvalidKeyException, SignatureException, NoSuchAlgorithmException, UnsupportedEncodingException, Exception{
     this.token = new Token(CompanyID);
        this.TokenSignature = RSASigningAndVerification.RSASigner(token.GetToken(),PrivateKeyReader.get(Location+CompanyID) );
        this.Time = getCurrentTimeStamp();
        this.client = client;
        AlphaToken = token.GetToken()+token.GetToken().length()+TokenSignature+TokenSignature.length()+Time+client.getClientIDString()+client.getClientIDString().length();
        }
    
    public AlphaToken() {
    }
   
    
    public String getAlphaToken() throws NoSuchAlgorithmException{

        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(256); // for example
        SecretKey secretKey = keyGen.generateKey();  
        
        
        //TODO Save the key in a file or load the key from a filew
    return AESEncryptor.encrypt(secretKey.getEncoded().toString(), "RandomInitVector", AlphaToken);
    }
    
    
    
    private static String getCurrentTimeStamp() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    Date now = new Date();
    String strDate = sdf.format(now);
    return strDate;
}
}
