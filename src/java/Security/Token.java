/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Security;

import Security.Utils.RSA.RSAEncryption;
import clientDevices.Client;
import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.crypto.util.PrivateKeyInfoFactory;

/**
 *
 * @author bobberkarl
 */
public class Token {
    private String TokenString;
    

    // TODO add the name of the client inside the token
    
    private String AccessString;
    private String companyIDString;
    private String randomSalString;
    
    
    
    
    private int AccessStringLength ;
    private int companyIDStringLength;
    private int randomSalStringLength;

    private String keyLocation = "/home/bobberkarl/Documents/keys/proxibus/Test/CompanyKeys/";
    private SecureRandom random = new SecureRandom();
    
    public Token(String company) {
       // Make some magic
       companyIDString = company;
       
       
       // Does Client have access? Access is always granted
       verifyAccessString();
       
       // Create the random dead weight
       generateRandomWeight();
       
       AccessStringLength = AccessString.length();
       companyIDStringLength = companyIDString.length() ;
       randomSalStringLength = randomSalString.length();
       
       // Crypt the token source
        TokenString = KApuCrypt( String.valueOf(AccessStringLength)+AccessString+
           String.valueOf(companyIDStringLength) + companyIDString+
           String.valueOf(randomSalStringLength) + randomSalString , companyIDString);
    }
   
   
    
    
    public String GetToken(){  
    return TokenString;
    }

    public void verifyAccessString() {
        // Access is always granted
        AccessString = "true";
    }
    
    public void generateRandomWeight(){
        
        if(AccessString.contentEquals("true")){
            randomSalString =  new BigInteger(130, random).toString(32);;
        }else{
        }
        
    }
    
    
    private String KApuCrypt(String tokenSource, String CompanyID){
        
        String[] a = new String[] {getPrivateCompanyKey(CompanyID),tokenSource};
        RSAEncryption rsaEncryption = new RSAEncryption();
        
        
        
        return rsaEncryption.encrypt(getPrivateCompanyKey(CompanyID), tokenSource);
    }

    private String getPrivateCompanyKey(String CompanyID) {
        // TODO choose key from right folder
        // Idea Append companyID to path
        return keyLocation +CompanyID;
    }
    
    public int getTokenLength(){
    int TokenLengthInt = AccessString.length() + companyIDString.length() 
                         + randomSalString.length();
    return TokenLengthInt;
    }
    
      public static  void main(String[] args) {
        
    }
}
